var newsApp = angular.
module('newsApp').
config(['$routeProvider','$locationProvider',
	    function ($routeProvider, $locationProvider) {
		$routeProvider
		.when('/news',{
			template: '<news-list></news-list>'
		})
		.when('/news/update/:id',{
			template: '<news-update></news-update>'
		})
		.when('/news/add',{
			template: '<news-add></news-add>'
		})
		.when('/news/:id',{
			template: '<news-page></news-page>'
		})
		.when('/author',{
			template: '<authors-list></authors-list>'	
		})
		.when('/tag',{
			template: '<tags-list></tags-list>'	
		})
		.when('/error/:code',{
			template: '<error-page></error-page>'
		})
		.otherwise('/news');

	}
]);

newsApp.run(['$rootScope','$location', function($rootScope, $location){
    var history = [];

    $rootScope.$on('$routeChangeSuccess', function() {
        history.push($location.$$path);
        if(history.length > 2){
        	history = history.splice(-2);
        }
    });

   	$rootScope.back = function() {
        var previousUrl = history.length > 1 ? history[history.length-2] : "/";
        $location.path(previousUrl);
    };
}]);