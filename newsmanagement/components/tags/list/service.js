angular.module('tagsList')
.factory('TagService', ['$resource', function($resource){
	return $resource('http://localhost:8084/news-rest-controller/tag/:oldTag/:tag/:number/:page', { oldTag: '@oldTag' }, {
	    update: {
	     	method: 'PUT'
        }
	},
	{
	    delete: {
	      	method: 'DELETE'
	    }
	});
}]);