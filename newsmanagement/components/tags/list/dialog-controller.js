tagsList.controller('TagsDialogController', ['$modalInstance','windowTitle',
 function ($modalInstance,windowTitle) {
	var self = this;
	this.windowTitle = windowTitle;
	
	this.cancel = function() {
		$modalInstance.close(false);
	}
	this.ok = function(){
		$modalInstance.close(true);
	}
	this.update = function(){
		$modalInstance.close(self.tag);
	}
}]);