angular.
module('tagsList')
.component('tagsList', {
	templateUrl: 'components/tags/list/template.html',
	controller: ['TagService','$location','$modal', '$http',
	function TagsListController(TagService, $location, $modal, $http){
		var self = this;
		if(self.number===undefined){
			self.number = 10;
		}
		if(self.page===undefined) {
			self.page = 1;
		}
		
		updateTagPage();
		
		function updateTagPage(){
			var fetchedTags = TagService.query({number:self.number,page:self.page},
				function(){
					self.tagsList = fetchedTags;
				},errorHandler);
			self.nextPageExist = self.page>1;
			var requiredNewsPosition = self.number * self.page + 1;
			var fetchedOneTag = TagService.query({number:1, page: requiredNewsPosition}, 
				function(){
					self.previousPageExist = ((fetchedOneTag !== undefined) && (fetchedOneTag.length > 0));
				},errorHandler);
		}

		self.nextPage = function(){
			self.page = self.page > 1 ? +self.page - 1 : 1;
			updateTagPage();
		}

		self.previousPage = function(){
			self.page = self.previousPageExist ? +self.page + 1 : self.page;
			updateTagPage();
		}
		
		self.saveTag = function(){
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/tag-dialog.template.html',
				controller: 'TagsDialogController',
				controllerAs: '$ctrl',
				resolve:{
					windowTitle: function(){
						return 'Add tag';
					}
				}
			});
			dialog.result.then(acceptSave);
			
		}

		var acceptSave = function(tag){
			if(tag!==false){
				TagService.save(tag, function(){
					updateTagPage();
					self.successMessage='Tag has been saved';
				},errorHandler);
			}
		}

		self.deleteTag = function(tag){
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/accept-dialog.template.html',
				controller: 'TagsDialogController',
				controllerAs: '$ctrl',
				resolve: {
					windowTitle: function(){
						return 'Delete tag';
					},
					deletedTag: function (){
						return tag;
					}
				}
			});
			dialog.result.then(function (isDeleted){
				if(isDeleted){
					TagService.delete(tag, function(){
					updateTagPage();
					self.successMessage='Tag has been deleted';
				}, errorHandler);
				}
			});
		}
		
		self.updateTag = function(tag){
			var oldTag = tag;
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/tag-dialog.template.html',
				controller: 'UpdateTagDialogController',
				controllerAs: '$ctrl',
				resolve: {
					tag: function (){
						return tag;
					},
					windowTitle: function(){
						return 'Update tag';
					}
				}
			});

			dialog.result.then(function (newTag){
				if(newTag!==false){
					var putTagUrl = 'http://localhost:8084/news-rest-controller/tag/'+oldTag.tag+'/'+newTag;
					$http.put(putTagUrl)
						.then(function(){
						updateTagPage();
						self.successMessage='Tag has been updated';
					},errorHandler);
				}
			});
		}
		
		var errorHandler = function (error){
			switch(error.status){
				case 409:
					self.errorMessage = 'Cannot remove this tag';
					break;
				case 422:
					self.errorMessage = 'Cannot insert duplicate tag';
					break;
				default:
					$location.path('/error/'+error.status);
			}
		}
	}]
});