tagsList.controller('UpdateTagDialogController', ['$modalInstance','tag', 'windowTitle',
 function ($modalInstance, tag, windowTitle) {
	var self = this;
	this.tag = tag.tag;
	this.windowTitle = windowTitle;
	
 	this.cancel = function() {
		$modalInstance.close(false);
	}
	this.ok = function(){
		$modalInstance.close(true);
	}
	this.update = function(){
		$modalInstance.close(self.tag);
	}
}]);