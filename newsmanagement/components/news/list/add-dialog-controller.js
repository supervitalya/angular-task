angular.module('newsList')
	.controller('NewsAddDialogController', ['$modalInstance','authors','tags', 'news','windowTitle',
		function ($modalInstance, authors, tags, news, windowTitle) {
	var self = this;
	this.authorsList = authors;
	this.tagsList = tags;
	this.authors=[];
	this.tags=[];
	this.windowTitle = windowTitle;

	if(news === null){
		this.news ={authors:[],tags:[]};
	}else{
		this.news = news;
		removeDuplicatesFromAuthorsDropdown();
		removeDuplicatesFromTagsDropdown();
	}

	this.cancel = function() {
		$modalInstance.close(false);
	}
	this.save = function(){
		$modalInstance.close(this.news);
	}
	
	self.addAuthor = function(author){
		self.news.authors.push(author);
		var authorIndex = self.authorsList.indexOf(author);
		self.authorsList.splice(authorIndex,1);
	}
	self.addTag = function(tag){
		self.news.tags.push(tag);
		var tagIndex = self.tagsList.indexOf(tag);
		self.tagsList.splice(tagIndex,1);
	}

	self.removeAuthor = function(author){
		var authorIndex = self.news.authors.indexOf(author);
		self.news.authors.splice(authorIndex,1);
		self.authorsList.push(author);
		self.authorsList.sort(authorsComparator);
	}

	self.removeTag =  function(tag){
		var tagIndex = self.news.tags.indexOf(tag);
		self.news.tags.splice(tagIndex,1);
		self.tagsList.push(tag);
		self.tagsList.sort(tagsComparator);
	}

	var authorsComparator = function(a,b){
		if (a.firstName < b.firstName)
			return -1;
		if (a.firstName > b.firstName)
			return 1;
		return 0;
	}

	var tagsComparator = function(a,b){
			if (a.tag < b.tag)
				return -1;
			if (a.tag > b.tag)
				return 1;
			return 0;
	}

	function removeDuplicatesFromAuthorsDropdown(){
		var newsAuthors = self.news.authors;
		var allAuthors = self.authorsList;

		for(var i=0; i < newsAuthors.length; i++){
			for(var j=0; j < allAuthors.length; j++){
				if (allAuthors[j].id === newsAuthors[i].id){
					self.authorsList.splice(j, 1);
				}
			}
		}
	}

	function removeDuplicatesFromTagsDropdown(){
		var newsTags = self.news.tags;
		var allTags = self.tagsList;

		for(var i=0; i < newsTags.length;i++){
			for(var j=0; j < allTags.length; j++){
				if (allTags[j].tag === newsTags[i].tag){
					self.tagsList.splice(j, 1);
				}
			}
		}
	}
}]);