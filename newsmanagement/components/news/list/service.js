angular.
module('newsList')
	.factory('NewsService',	['$resource', function($resource){
		return $resource('http://localhost:8084/news-rest-controller/news/:id/:criteria/:criteriaValue/:number/:page', { id: '@id' }, {
		    update: {
		      method: 'PUT'
		    }
		},
		{
		    delete: {
		      method: 'DELETE'
		    }
		});
	}]);

