angular.module('newsList')
	.controller('NewsDialogController', ['$modalInstance', 
		function ($modalInstance) {
	var self = this;
	
	this.cancel = function() {
		$modalInstance.close(false);
	}
	this.ok = function(){
		$modalInstance.close(true);
	}

}]);