
angular.module('newsList')
.controller('NewsListController', ['NewsService','TagService','CommentService','AuthorService','$location','$modal', 
	function (NewsService, TagService, CommentService, AuthorService, $location, $modal){
		var self = this;
		
		if(self.number===undefined) {
			self.number = 10;
		}
		if(self.page===undefined) {
			self.page = 1;
		}

		updatePage();
		
		var fetchedTags = TagService.query({tag:'tags'},
			function(){
				self.tagsList = fetchedTags;
			},errorHandler);
		var fetchedAuthors = AuthorService.query({id:'authors'},
			function(){
				self.authorsList = fetchedAuthors;
			},errorHandler);
	
		
		function updatePage(){
			var fetchedNews = NewsService.query(
				{	criteria:self.searchBy, 
					criteriaValue: self.searchingCriteria,
					number: +self.number +1,
					page:self.page
				}, function(){
					self.nextPageExist = self.page > 1;
					self.newsList = fetchedNews;
					if ((self.searchBy === 'top') && (self.number*self.page >= self.searchingCriteria)){
						self.previousPageExist = false;
					}else if(self.newsList.length < +self.number+1){
						self.previousPageExist = false;
					}else{
						self.newsList.splice(-1,1);
						self.previousPageExist = true;
					}
				},errorHandler);
		}
		
		self.changeNewsNumber = function(){
			self.page = 1;
			updatePage();
		}

		self.nextPage = function(){
			self.page = self.page > 1 ? +self.page - 1 : 1;
			updatePage();
		}

		self.previousPage = function(){
			self.page = self.previousPageExist ? +self.page + 1 : self.page;
			updatePage();
		}

		self.findByTag = function (tag){
			self.searchBy = 'tag';
			self.searchingCriteria = tag;
			updatePage();
		}

		self.findByAuthorId = function (authorId){
			self.searchBy = 'author';
			self.searchingCriteria = authorId;
			updatePage();
		}

		self.topByComments = function (newsNumber){
			self.searchBy = 'top';
			self.searchingCriteria = newsNumber;
			updatePage();
		}

		self.deleteNews = function(newsId){
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/accept-dialog.template.html',
				controller: 'NewsDialogController',
				controllerAs: '$ctrl',
			});
			dialog.result.then(function (accepted){
				acceptDelete(accepted, newsId);
			});
		}

		var acceptDelete = function(accepted, newsId){
			if(accepted){
				NewsService.delete({id:newsId}, function(){
					updatePage();	
				},errorHandler);
			}
		}

		self.addNews = function(){
			var allAuthors = AuthorService.query(function(){},errorHandler);
			var allTags = TagService.query(function(){},errorHandler);
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/news-dialog.template.html',
				controller: 'NewsAddDialogController',
				controllerAs: '$ctrl',
				resolve: {
					windowTitle: function(){
						return 'Add news:';
					},
				    tags: function () {
				        return allTags;
				    },
				    authors: function () {
				        return allAuthors;
				    },
				    news: function(){
				    	return null;
				    }
				}
			});
			dialog.result.then(function (news){
				if(news!==false){
				acceptAdd(news);
				}
			});
		}

		var acceptAdd = function(news){
			var currentTimestamp = new Date().getTime();
			news.creationDate = currentTimestamp;
			news.modificationDate = currentTimestamp;
			NewsService.save(news,
			function(){
				updatePage();
			},errorHandler);
		}

		self.updateNews = function(news){
			var allAuthors = AuthorService.query(function(){},errorHandler);
			var allTags = TagService.query(function(){},errorHandler);
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/news-dialog.template.html',
				controller: 'NewsAddDialogController',
				controllerAs: '$ctrl',
				resolve: {
					windowTitle:function(){
						return 'Update news:';
					},
				    tags: function () {
				        return allTags;
				    },
				    authors: function () {
				        return allAuthors;
				    },
				    news: function(){
				    	return news;
				    }
				}
			});
			dialog.result.then(function (news){
				if(news!==false){
					acceptUpdate(news);
				}
			});
		}

		var acceptUpdate = function(news){
			var currentTimestamp = new Date().getTime();
			news.creationDate = currentTimestamp;
			news.modificationDate = currentTimestamp;
			NewsService.update(news,
			function(){
				updatePage();
			},errorHandler);
		}

		var errorHandler = function (error){
			$location.path('/error/'+error.status);
		}
	}]);