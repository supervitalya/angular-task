angular.module('newsPage')
.factory('CommentService',['$resource', function($resource){
	return $resource('http://localhost:8084/news-rest-controller/comment/:newsId/:commentId/:number/:page',
	 { commentId: '@id' },	{
		delete: {
			method: 'DELETE'
		}
	});
}]);
