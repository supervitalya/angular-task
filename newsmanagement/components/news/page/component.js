angular.
module('newsPage')
.component('newsPage', {
	templateUrl: 'components/news/page/template.html',
	controller: ['CommentService','NewsService','$routeParams','$location', '$modal',
	function NewsPageController(CommentService, NewsService, $routeParams, $location, $modal){
		var self = this;
		var newsId = $routeParams.id;
	
		self.news = {comments:[],authors:[],tags:[]};

		if(self.number===undefined) {
			self.number = 5;
		}
		if(self.page===undefined) {
			self.page = 1;
		}
		
		var fetchedNews = NewsService.get({id:newsId},
			function(){
				self.news = fetchedNews;
				updateComments();
			},errorHandler);
		
		function updateComments(){
			var fetchedComments = CommentService.query({newsId: newsId, number:self.number, page:self.page},
				function(){
					self.comments = fetchedComments;
					self.nextPageExist = self.page > 1;
				},errorHandler);
			var requiredPageNumber = self.page * self.number + 1;
			var previousOneComment = CommentService.query({newsId:newsId, number:1, page:requiredPageNumber},
				function(){
					self.previousPageExist = ((previousOneComment !== undefined) && (previousOneComment.length > 0));
				},errorHandler);
		}

		self.nextPage = function(){
			self.page = self.page > 1 ? +self.page - 1 : 1;
			updateComments();
		}

		self.previousPage = function(){
			self.page = self.previousPageExist ? +self.page + 1 : self.page;
			updateComments();
		}

		self.addComment = function(){
			var currentTimestamp = new Date().getTime();
			var comment = {
				commentText: self.newComment,
				creationDate: currentTimestamp,
				newsId: self.news.id
			};
			CommentService.save(comment, 
				function(){
					updateComments();
					self.newComment = null;
				},errorHandler);
		}

		self.deleteComment = function(comment){
				var dialog = $modal.open({
				templateUrl: 'components/dialogs/accept-dialog.template.html',
				controller: 'NewsDialogController',
				controllerAs: '$ctrl',
			});
			dialog.result.then(
				function(accepted){
					if(accepted){
						CommentService.delete({commentId: comment.id},
						function(){
							updateComments();
						});
					}
				}
			);
		}
		
		var errorHandler = function (error){
			$location.path('/error/'+error.status);
		}
	}
	]
});