angular.
module('errorPage')
.component('errorPage', {
	templateUrl: 'components/error-page/template.html',
	controller: ['$routeParams', function ErrorPageController($routeParams){
		this.errorCode = $routeParams.code;
		var self = this;
		switch(this.errorCode){
			case '500':
				self.message = 'Internal server error. Please try again later. Sorry for the inconvenience.';
				break;
			case '400':
				self.message = 'Requested resource not found. Please, check the request.';
				break;
			default:
				self.message = 'Unknown error.';
		}
	}
	]
});