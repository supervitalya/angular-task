authorsList.controller('UpdateAuthorDialogController', ['$modalInstance','author', 'windowTitle',
	function ($modalInstance, author, windowTitle) {
	var self = this;
	this.author = author;
	this.windowTitle = windowTitle;

	this.cancel = function() {
		$modalInstance.close(false);
	}
	this.save = function(){
		$modalInstance.close(this.author);
	}
}]);