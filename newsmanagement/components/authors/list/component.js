angular.
module('authorsList')
.component('authorsList', {
	templateUrl: 'components/authors/list/template.html',
	controller: ['AuthorService','$location','$modal', 
	function AuthorsListController(AuthorService, $location, $modal){
		var self = this;
		if(self.number===undefined){
			self.number = 10;
		}
		if(self.page===undefined) {
			self.page = 1;
		}
	
		updateAuthorPage();

		function updateAuthorPage(){
			var fetchedAuthors = AuthorService.query({number:self.number, page:self.page},
				function(){
					self.authorsList = fetchedAuthors;
				},errorHandler);

			self.nextPageExist = self.page > 1;
			var requiredNewsPosition = self.number * self.page + 1;
			var previousOneAuthor = AuthorService.query({number: 1, page: requiredNewsPosition},
				function(){
					self.previousPageExist = ((previousOneAuthor !== undefined) && (previousOneAuthor.length > 0));
				},errorHandler);
		}

		self.nextPage = function(){
			self.page = self.page > 1 ? +self.page - 1 : 1;
			updateAuthorPage();
		}

		self.previousPage = function(){
			self.page = self.previousPageExist ? +self.page + 1 : self.page;
			updateAuthorPage();
		}

		self.saveAuthor = function(){
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/author-dialog.template.html',
				controller: 'AddAuthorDialogController',
				controllerAs: '$ctrl',
				resolve:{
					windowTitle: function(){
						return 'Add author'
					}
				}
			});
			dialog.result.then(acceptSave);
		}

		var acceptSave = function(data){
			if(data!=false){
				var author={};
				author.firstName = data.firstName;
				author.lastName = data.lastName;
				AuthorService.save(author,
				function(){
					updateAuthorPage();
						self.successMessage='Author has been saved';
			},errorHandler);
			}
		}
		
		self.updateAuthor = function(author){
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/author-dialog.template.html',
				controller: 'UpdateAuthorDialogController',
				controllerAs: '$ctrl',
				resolve: {
					windowTitle: function(){
						return 'Update author'
					},
					author: function () {
				        return author;
				    }
				}
			});
			dialog.result.then(acceptUpdate);
		}

		var acceptUpdate = function(data){
			if(data!== false){
				AuthorService.update(data,
					function(){
						updateAuthorPage();
						self.successMessage='Author has been updated';
					},errorHandler);
			}
		}

		self.deleteAuthor = function(author){
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/accept-dialog.template.html',
				controller: 'AddAuthorDialogController',
				controllerAs: '$ctrl',
				resolve: {
					windowTitle: function(){
						return 'Delete author'
					}
				}				
			});
			dialog.result.then(function(accepted){
				acceptDelete(accepted, author);
			});
		}

		var acceptDelete = function (accepted, author){
			if(accepted){
				AuthorService.delete(author, function(){
				updateAuthorPage();
						self.successMessage='Author has been deleted';
				},errorHandler);
			}
		}

		var errorHandler = function (error){
			switch(error.status){
				case 409:
					self.errorMessage = 'Cannot remove this author';
					break;
				default:
					$location.path('/error/'+error.status);
			}
		}
	}]
});