angular.module('authorsList')
.factory('AuthorService',['$resource', function($resource){
	return $resource('http://localhost:8084/news-rest-controller/author/:id/:number/:page', { id: '@id' }, {
		update: {
			method: 'PUT'
		}
	},
	{
		delete: {
			method: 'DELETE'
		}
	});
}]);
