authorsList.controller('AddAuthorDialogController', ['$modalInstance', 'windowTitle',
	function ($modalInstance, windowTitle) {
	var self = this;
	this.author;
	this.windowTitle = windowTitle;
	this.ok = function(){
		$modalInstance.close(true);
	}
	this.cancel = function() {
		$modalInstance.close(false);
	}
	this.save = function(){
		$modalInstance.close(self.author);
	}
}]);